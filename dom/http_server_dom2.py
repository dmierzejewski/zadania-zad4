# -*- encoding: utf-8 -*-

import socket
import email.utils
import os

def http_serve(server_socket):
    base_path = 'web' #sciezka domyslnego katalogu, wzgledem ktorego wyznaczamy adresy
    while True:
        # Czekanie na polaczenie
        connection, client_address = server_socket.accept()
        try:
            # Odebranie zadania
            request = connection.recv(1024)
            if request:
                print "Odebrano:"
                print request #serwer drukuje sobie żądanie
                requesttable = request.split("\r\n") #dzielimy żądanie
                first_line_req = requesttable[0].split(" ") # i pierwszą linię żądania
                #warunek sprawdzający poprawność żądania HTTP
                if len(first_line_req)==3 and (str(first_line_req[0])=="GET") and (str(first_line_req[2][:4])=="HTTP"):
                    uri = first_line_req[1]
                    print uri
                    if os.path.exists(base_path+uri): #sprawdzamy czy istnieje zasób do któego chcemy się odwołać
                        header_status = "HTTP/1.1 200 OK\r\n" #zasób istnieje, zwracamy sukces
                        if uri[-5:]=='.html' or uri[-4:]=='.htm': #dla dokumentu html
                            html = open(base_path+uri).read() #dane
                            header_contenttype="Content-Type: text/html; charset=UTF-8\r\n" #MIME Type danych - odpowiedni header
                            header_length="Content-Length: "+ str(len(html))+"\r\n" # długość danych
                        elif uri[-4:]=='.png': # dla obrazu .png
                            html = file(base_path+uri, 'rb').read() # czytamy binarnie
                            header_contenttype="Content-Type: image/png\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif uri[-4:]=='.txt': # dla plku tekstowego
                            html = open(base_path+uri).read()
                            header_contenttype="Content-Type: text/plain; charset=UTF-8\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif uri[-4:]=='.jpg' or uri[-5:]=='.jpeg': # dla obrazu jpg
                            html = file(base_path+uri, 'rb').read()
                            header_contenttype="Content-Type: image/jpeg\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        elif os.path.isdir(base_path+uri): # dla folderu
                            html='<html><head></head><body><ul>' # zawartośc zostanie wylistowana
                            dir_list = os.listdir(base_path+uri) # zawartosc folderu
                            #print dir_list
                            for filename in dir_list:
                                if os.path.isdir(base_path+uri+filename): #foldery wypisujemy z / na końcu
                                    html+='<li><a href="'+uri+filename+'/">'+filename+'/</a></li>'
                                else:
                                    html+='<li><a href="'+uri+filename+'">'+filename+'</a></li>'
                            html+='</ul></body></html>'
                            header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                        else: # dla nieobsługiwanego typu danych - traktuje jako plik który mozna ściągnąć
                            html = file(base_path+uri, 'rb').read()
                            header_contenttype="Content-Type: application/octet-stream\r\n"
                            header_length="Content-Length: "+ str(len(html))+"\r\n"
                    else: # dla nieistniejacego zasobu
                        header_status = "HTTP/1.1 404 Not Found\r\n"
                        html = '<html><head></head><body><h4>404 Not Found</h4><p>Nie ma takiego numeru. Zasobu też.</p></body></html>'
                        header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                        header_length="Content-Length: "+ str(len(html))+"\r\n"
                else: # dla błędnego requestu
                    header_status = "HTTP/1.1 400 Bad Request\r\n"
                    html = "Uzytkowniku, jestes bardzo zlym czlowiekiem. To jest serwer HTTP i przyjmuje zadania HTTP. Prosimy do okienka nr 4b."
                    header_contenttype="Content-Type: text/html; charset=UTF-8\r\n"
                header_date ="GMT-Date: "+email.utils.formatdate(usegmt=True)+"\r\n" # data w odpowiednim formacie
                # wysyłamy nagłówki + dane
                try:
                    connection.sendall(header_status+header_contenttype+header_date+header_length+"\r\n"+html)
                except socket.error: #zabezpieczenie przed Broken Pipe
                    continue

        finally:
            # Zamkniecie polaczenia
            connection.close()


# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego uzycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
#server_address = ('localhost', 4000)  # wersja działająca na localhoście
server_address = ('194.29.175.240', 31013) # wersja mogaca działać (i działająca) na serwerze
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)



try:
    http_serve(server_socket)

finally:
    server_socket.close()

